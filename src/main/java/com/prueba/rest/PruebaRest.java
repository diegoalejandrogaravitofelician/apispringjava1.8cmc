package com.prueba.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.dao.PersonaDAO;
import com.prueba.models.Usuario;

@RestController
@RequestMapping("usuarios")
public class PruebaRest {
	
	@Autowired
	private PersonaDAO PersonaDAO ;
	
	//Solicitud HTTP GET
	@GetMapping("/consultar")
	public List <Usuario> consultar(){
		return  PersonaDAO.findAll();
	}
	
	//SOlicitud HTTP POST
	@PostMapping("/agregar")
	public void guardarUsuario(@RequestBody Usuario usuario) {
		PersonaDAO.save(usuario);
	}
	
	//Solicitud  HTTP PUT 
	@PutMapping("/actualizar")
	public void actualizar (@RequestBody Usuario usuarios) {
		PersonaDAO.save(usuarios);
	}
	
	//solicitud HTTP DELTE
	@DeleteMapping("/eliminar/{id}")
	public void Eliminar(@PathVariable("id") Integer id) {
		PersonaDAO.deleteById(id);
	}
	


}
