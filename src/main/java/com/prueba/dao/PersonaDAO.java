package com.prueba.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.prueba.models.Usuario;

public interface PersonaDAO  extends JpaRepository<Usuario, Integer>{

}
